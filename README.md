# React Template
[![ReactJS](https://i.imgur.com/Cka06kf.png)](https://reactjs.org/)
[![create-react-app](https://badge.fury.io/js/create-react-app.svg)](https://facebook.github.io/create-react-app/)

Simple React boilerplate for GitLab Pages

## Getting Started

Create a React project from template

Clone the project to your machine
```
git clone https://gitlab.com/USERNAME/PROJECT.git
```

Navigate to the project folder
```
cd PROJECT
```

Run the setup to get everything up and running
```
npm install
```

Inside **package.json**, change the *homepage* to your own URL
```
"homepage": "http://USERNAME.gitlab.io/PROJECT"
```

And start the development server
```
npm start
```

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


## More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).