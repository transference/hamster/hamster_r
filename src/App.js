import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Contacts from './components/contacts';


class App extends Component {
  state = {
    contacts: []
  }
  componentDidMount() {
    fetch('https://jsonplaceholder.typicode.com/users')
    .then(res => res.json())
    .then((data) => {
      this.setState({ contacts: data })
    })
    .catch(console.log)
  }
  render() {
    return (
      <div className="App">
        <img src='https://gitlab.com/transference/hamster/hamster_r/badges/master/pipeline.svg?style=flat-square'/>
        <Contacts contacts={this.state.contacts} />
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Transference Hamster React Beta
          </p>
          <a
            className="App-link"
            href="https://transference.gitlab.io/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
    );
  }
}

export default App;
